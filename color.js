"use strict";

const text = document.querySelectorAll('.text');

text.forEach(text => {
    text.addEventListener('keyup', textColor);
});


function textColor(e){
    if(e.keyCode === 82){
        text.forEach(text => {
            text.style.color = 'red';
        });
    }
    else if(e.keyCode === 71){
        text.forEach(text => {
            text.style.color = 'green';
        });
    }
    else if(e.keyCode === 66){
        text.forEach(text => {
            text.style.color = 'blue';
        });
    }
}