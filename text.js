"use strict";

window.onbeforeunload = () => {
    if(i){
        return "";
    }
}

let i = false;

let text = document.getElementById('text');
text.addEventListener('input', iTrue);

function iTrue(){
    i = true;
}

let save = document.getElementById('save');
save.addEventListener('click', iFalse);

function iFalse(){
    i = false;
}